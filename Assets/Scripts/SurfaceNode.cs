using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceNode : MonoBehaviour
{
    private float yVelocity;
    private float yForce;
    private float targetY;
    private float secondsUntilTriggerable;
    private float untriggerableDuration = 0.5f;
    private SurfaceNode leftNode;
    private SurfaceNode rightNode;

    public float Stiffness { get; set; }
    public float Firmness { get; set; }
    public float Damping { get; set; }
    public float CollisionFactor { get; set; }
    public float Speed { get; set; }

    private void Start()
    {
        targetY = transform.position.y;
    }

    private void Update()
    {
        CheckInteractive();
        UpdatePosition();
    }

    private void UpdatePosition()
    {
        float delta = Time.deltaTime * Speed;

        float springExtension = transform.position.y - targetY;
        float yDampingLoss = Damping * yVelocity;

        yForce = ((-springExtension * Stiffness));
        yForce += CalculateNeighborForce();
        yForce -= yDampingLoss;
        yForce *= delta;
        yVelocity += yForce;
        transform.position += new Vector3(0, yVelocity, 0);
    }

    private float CalculateNeighborForce()
    {
        float force = 0;
        if (leftNode != null)
        {
            float yDistance = leftNode.transform.position.y - transform.position.y;
            force += yDistance * Firmness;
        }
        if (rightNode != null)
        {
            float yDistance = rightNode.transform.position.y - transform.position.y;
            force += yDistance * Firmness;
        }
        return force;
    }

    private void CheckInteractive()
    {
        if (secondsUntilTriggerable == 0)       return;
        else if (secondsUntilTriggerable < 0)   secondsUntilTriggerable = 0;
        else                                    secondsUntilTriggerable -= Time.deltaTime;
    }

    public void SetPhysics(float stiffness, float firmness, float damping, float collisionFactor, float speed)
    {
        Stiffness = stiffness;
        Firmness = firmness;
        Damping = damping;
        CollisionFactor = collisionFactor;
        Speed = speed;
    }

    public void SetNeighbors(SurfaceNode leftNeighbor, SurfaceNode rightNeighbor)
    {
        leftNode = leftNeighbor;
        rightNode = rightNeighbor;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (secondsUntilTriggerable > 0) return;

        Rigidbody2D rb = collision.GetComponent<Rigidbody2D>();
        yVelocity += rb.velocity.y * CollisionFactor;
        //Splash(collision, rb.velocity.y * collisionVelocityFactor);
        secondsUntilTriggerable = untriggerableDuration;
    }
}
