using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float speed = 1f;

    private Rigidbody2D rb2D;

    private void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            rb2D.AddForce(new Vector2(0, 1) * Time.deltaTime * speed);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            rb2D.AddForce(new Vector2(0, -1) * Time.deltaTime * speed);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb2D.AddForce(new Vector2(-1, 0) * Time.deltaTime * speed);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb2D.AddForce(new Vector2(1, 0) * Time.deltaTime * speed);
        }
    }
}
