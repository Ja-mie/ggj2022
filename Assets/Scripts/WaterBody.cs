﻿using System.Collections.Generic;
using UnityEngine;

public class WaterBody : MonoBehaviour
{
    [Header("Prefabs")]
    [SerializeField] private GameObject surfaceNodePrefab;

    [Header("Appearance Settings")]
    [SerializeField] private int quality;
    [SerializeField] private GameObject splashParticle;
    [SerializeField] private Color waterColour;

    [Header("Physics Settings")]
    [SerializeField] private float stiffness = .02f;
    [SerializeField] private float firmness = 1;
    [SerializeField] private float damping = .1f;
    [SerializeField] private float collisionFactor = .04f;
    [SerializeField] private float speed = 1;

    private MeshRenderer meshRenderer;
    private MeshFilter meshFilter;
    private Mesh mesh;
    private Transform topLeft;
    private Transform topRight;
    private Transform bottomLeft;
    private Transform bottomRight;
    private List<SurfaceNode> surfaceNodes;
    private List<Vector3> vertices;

    private void Start()
    {
        meshRenderer = gameObject.GetComponent<MeshRenderer>();
        meshFilter = gameObject.GetComponent<MeshFilter>();
        meshRenderer.material.color = waterColour;
        SetupCorners();
        CreateSurfaceNodes();
        CreateMesh();
    }

    private void SetupCorners()
    {
        topLeft = transform.Find("TopLeft").transform;
        topRight = transform.Find("TopRight").transform;
        bottomLeft = transform.Find("BottomLeft").transform;
        bottomRight = transform.Find("BottomRight").transform;
    }

    private void CreateSurfaceNodes()
    {
        Vector3 surfaceVector = topRight.position - topLeft.position;
        Vector3 qualityUnitVector = surfaceVector / (quality-1);
        surfaceNodes = new List<SurfaceNode>();

        for (int i = 0; i < quality; i++)
        {
            Vector3 position = topLeft.position + (qualityUnitVector * i);
            SurfaceNode node = Instantiate(surfaceNodePrefab, position, Quaternion.identity, transform).GetComponent<SurfaceNode>();
            surfaceNodes.Add(node);
        }
        for (int i = 0; i < surfaceNodes.Count; i++)
        {
            SurfaceNode leftNode = (i > 0) ? surfaceNodes[i-1] : null;
            SurfaceNode rightNode = (i < surfaceNodes.Count-1) ? surfaceNodes[i+1] : null;
            surfaceNodes[i].SetPhysics(stiffness, firmness, damping, collisionFactor, speed);
            surfaceNodes[i].SetNeighbors(leftNode, rightNode);
        }
    }

    private void CreateMesh()
    {
        vertices = new List<Vector3>();
        List<int> triangles = new List<int>();

        bool IsInner = false;
        for (int i = 0; i < quality; i++)
        {
            vertices.Add(surfaceNodes[i].transform.position);
            vertices.Add(new Vector3(surfaceNodes[i].transform.position.x, bottomLeft.position.y, 0));

            if (vertices.Count >= 4)
            {
                int endIndex = vertices.Count - 1;
                triangles.Add(endIndex - 3);
                triangles.Add(IsInner ? endIndex - 2 : endIndex - 1);
                triangles.Add(IsInner ? endIndex - 1 : endIndex - 2);

                triangles.Add(endIndex - 1);
                triangles.Add(IsInner ? endIndex - 2 : endIndex);
                triangles.Add(IsInner ? endIndex : endIndex - 2);
            }
        }

        mesh = new Mesh();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        meshFilter.mesh = mesh;
    }

    private void Update()
    {
        UpdateSurface();
        CheckUpdatePhysics();
    }

    private void UpdateSurface()
    {
        for (int i = 0; i < quality; i++)
        {
            vertices[i*2] = surfaceNodes[i].transform.position - transform.position;
        }
        mesh.vertices = vertices.ToArray();
    }

    private void CheckUpdatePhysics()
    {
        if ((surfaceNodes[0].Stiffness == stiffness)
            && (surfaceNodes[0].Firmness == firmness)
            && (surfaceNodes[0].Damping == damping)
            && (surfaceNodes[0].CollisionFactor == collisionFactor)
            && (surfaceNodes[0].Speed == speed))
        {
            return;
        }
        foreach (SurfaceNode node in surfaceNodes)
        {
            node.SetPhysics(stiffness, firmness, damping, collisionFactor, speed);
        }
    }
}

